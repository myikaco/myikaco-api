# myika.co API/backend

## Dev
`fresh` to start live web server  

MUST create `.env` file at root (ignored by Git):
```
SERVE_PORT=8000
AUTH=brie123
```

`SERVE_PORT` used to determine which port to listen on
`AUTH` var must be passed in POST request JSON body to authorize.

## Docker 
`docker run --name api-app -p 8000:8000 api-img` - run standalone image from Dockerfile

`docker-compose up --build` - build custom image + run container