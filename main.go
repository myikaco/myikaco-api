package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/go-redis/redis/v8"
)

// DB
var ctx = context.Background()
var redisHost = os.Getenv("REDIS_HOST")
var redisPort = os.Getenv("REDIS_PORT")
var rdb *redis.Client

type jsonResponse struct {
	Msg  string `json:"message"`
	Body string `json:"body"`
}

type jsonEmailPostRequest struct {
	Auth  string
	Email string
}

func initRedis() {
	if redisHost == "" {
		redisHost = "127.0.0.1"
		fmt.Println("REDIS_HOST env var nil, using redis dev address -- " + redisHost)
	}
	if redisPort == "" {
		redisPort = "6379"
		fmt.Println("REDIS_PORT env var nil, using redis dev port -- " + redisPort)
	}
	rdb = redis.NewClient(&redis.Options{
		Addr: redisHost + ":" + redisPort,
	})
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	var data jsonResponse
	if r.Method != "GET" {
		data = jsonResponse{Msg: "Only GET Allowed", Body: "This endpoint only accepts GET requests."}
		w.WriteHeader(http.StatusUnauthorized)
	} else {
		data = jsonResponse{Msg: "привет сука", Body: "курица - дерьмо"}
		w.WriteHeader(http.StatusOK)
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(data)
	// w.Write([]byte(`{"msg": "привет сука"}`))
}

func createNewArticle(w http.ResponseWriter, r *http.Request) {
	var data jsonResponse
	if r.Method != "POST" {
		data = jsonResponse{Msg: "Only POST Allowed", Body: "This endpoint only accepts POST requests."}
		w.WriteHeader(http.StatusUnauthorized)
	} else {
		// decode data
		var emailReq jsonEmailPostRequest
		err := json.NewDecoder(r.Body).Decode(&emailReq)
		if err != nil || emailReq.Auth == "" || emailReq.Email == "" {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		// check emailReq.Auth with env var
		auth := os.Getenv("AUTH")
		if auth != emailReq.Auth {
			data = jsonResponse{Msg: "Authorization Invalid", Body: "Auth field value from body invalid."}
			w.WriteHeader(http.StatusUnauthorized)
		} else {
			// add emailReq.Email to redis db
			// TODO: check if already exists before adding
			_, err := rdb.HSet(ctx, emailReq.Email, "timeAdded", time.Now().Format("Mon, 2 Jan 2006 15:04:05 MST")).Result()
			if err != nil {
				fmt.Println("HSET error -- ", err.Error())
			}

			data = jsonResponse{Msg: "Email Added", Body: emailReq.Email}
			w.WriteHeader(http.StatusCreated)
		}
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(data)
}

func main() {
	initRedis()

	muxRouter := http.NewServeMux()
	muxRouter.HandleFunc("/", indexHandler)
	muxRouter.HandleFunc("/email", createNewArticle)

	auth := os.Getenv("AUTH")
	fmt.Println("AUTH var = " + auth)

	port := os.Getenv("SERVE_PORT")
	fmt.Println("myikaco-api listening on port " + port)
	log.Fatal(http.ListenAndServe(":"+port, muxRouter))
}
